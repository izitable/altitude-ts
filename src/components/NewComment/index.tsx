import * as React from "react";
import * as Types from "./../../types/types";

const NewComment = (props: Types.NewCommentProps) => (
  <div className="row-post-comment">
    <div className="post-comment">
      <form acceptCharset="UTF-8" className="new-comment" id="new_comment">
        <div className="text">
          <label htmlFor="comment">New Comment</label>
          <input
            onChange={e => props.handleNicknameChange(e)}
            type="text"
            className="form-control"
            name="name"
            placeholder="Nickname"
          />
          <textarea
            onChange={e => props.handleCommentChange(e)}
            className="form-control"
            name="comment[body]"
            placeholder="Comment..."
          />
        </div>
        <div className="altitude-DialogActionBar">
          <a
            onClick={() => props.submitComment()}
            className="altitude-DialogButton"
          >
            POST COMMENT
          </a>
        </div>
      </form>
    </div>
  </div>
);

export default NewComment;
