import * as React from "react";
import * as Types from "./../../types/types";

const Modal = (props: Types.ModalProps) => (
  <div>
    <div className="altitude-DialogContent">
      <div className="altitude-DialogContentBody">
        <h4> New fun fact about coding </h4>
      </div>
    </div>
    <div className="post-comment">
      <form className="new-post-form">
        <div className="text">
          <textarea
            onChange={e => props.handleTextChange(e)}
            placeholder={"Fun facts about coding here ..."}
          />
        </div>
        <div className="form-group">
          <input
            onChange={e => props.handleNicknameChange(e)}
            type="email"
            className="form-control"
            placeholder="Nickname"
          />
        </div>
        <select
          className="form-control post-category"
          onChange={e => props.handleCategoryChange(e)}
        >
          <option value="" disabled selected hidden>
            Choose a category
          </option>
          {props.options.map((item: string) => <option>{item}</option>)}
        </select>
      </form>
    </div>
    <div className="altitude-DialogActionBar">
      <a onClick={() => props.submit()} className="altitude-DialogButton">
        Submit
      </a>
      <a onClick={() => props.cancel()} className="altitude-DialogButton">
        Cancel
      </a>
    </div>
  </div>
);

export default Modal;
