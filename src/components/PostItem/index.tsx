import * as React from "react";
import * as moment from "moment";
import * as Types from "./../../types/types";

const Posts = (props: Types.PostItemProps) => (
  <div>
    <header>
      <div className="author-post">
        <p className="author-name">{props.post.nickname}</p>
        <time className="author-post-time">
          {moment(props.post.date).format("MMM d, YYYY")}
        </time>
      </div>
    </header>
    <article>
      <p>{props.post.text}</p>
      <footer>
        <p
          className="funny"
          onClick={() =>
            props.updatePost !== undefined
              ? props.updatePost("funny", props.post)
              : console.log("empty")}
        >
          <span> {props.post.funny} </span>Funny
        </p>
        <p
          onClick={() =>
            props.updatePost !== undefined
              ? props.updatePost("boring", props.post)
              : console.log("empty")}
          className="boring"
        >
          <span> {props.post.boring} </span>Boring
        </p>
        <a
          onClick={() =>
            props.goToComments !== undefined
              ? props.goToComments(props.post)
              : console.log("empty")}
          className="comments"
        >
          {props.post.comments.length} Comments
        </a>
      </footer>
      <hr />
    </article>
  </div>
);

export default Posts;
