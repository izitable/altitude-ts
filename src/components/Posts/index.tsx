import * as React from "react";
import PostItem from "./../PostItem/index";
import * as Types from "./../../types/types";

const Posts = (props: Types.PostsProps) => (
  <div>
    {props.posts.map((item: Types.Post) => {
      return (
        <div className="post">
          <PostItem
            goToComments={props.goToComments}
            updatePost={props.updatePost}
            post={item}
          />
        </div>
      );
    })}
  </div>
);

export default Posts;
