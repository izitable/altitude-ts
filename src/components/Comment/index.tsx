import * as React from "react";
import * as moment from "moment";
import * as Types from "./../../types/types";

const Comment = (props: Types.CommentProps) => (
  <div>
    <div className="row-comment">
      <header>
        <div className="author-post">
          <p className="author-name">{props.comment.author}</p>
          <time className="author-post-time">
            {moment(props.comment.date).format("MMM d, YYYY")}
          </time>
        </div>
      </header>
      <article>
        <p>{props.comment.text}</p>
      </article>
      <hr />
    </div>
  </div>
);

export default Comment;
