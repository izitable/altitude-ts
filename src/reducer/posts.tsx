import {SUBMIT_POST, UPDATE_POST, SELECT_POST} from './../action/postActions';
import * as Types from './../types/types';



const initialState: Types.PostsState = {
  posts: [
    {
      text:
        "Ctrl C, Ctrl V, and Ctrl-Z have saved more lives than batman.Yes, Batman, it is true. The amount of copied code is hundred times more than typing a code ",
      comments: [],
      category: "Programming",
      nickname: "Jay",
      funny: 0,
      boring: 0,
      date: new Date( 1370001284000 )
    }
  ],
  categories: ["Logic", "Programming", "Testing"],
  selectedPost: undefined
};



export default function posts(state: Types.PostsState = initialState, action: Types.Action) {
  switch (action.type) {
    case SUBMIT_POST:
      return {...state, posts:state.posts.concat([action.payload])}
    case UPDATE_POST:
      var newPost = action.payload;
      return {
        ...state, 
        posts: state.posts.map((content : Types.Post, i) => 
        content.text === newPost.text && 
        content.nickname === newPost.nickname ? newPost : content)}
    case SELECT_POST:
      return {
        ...state,
        selectedPost: action.payload
      }
    default:
      return state;
  }
}
