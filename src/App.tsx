import * as React from "react";
import "./App.css";
import PostList from "./pages/PostList/index";
import PostDetails from "./pages/PostDetails/index";
import * as Types from "./types/types";

class App extends React.Component<{}, Types.AppState> {
  constructor(props: Types.AppProps) {
    super(props);
    this.state = {
      page: "list"
    };
  }

  getPage = () => {
    if (this.state.page === "list") {
      return <PostList changePage={this.changePage} />;
    } else if (this.state.page === "details") {
      return <PostDetails changePage={this.changePage} />;
    }
    return undefined;
  };

  changePage = () => {
    if (this.state.page === "list") {
      this.setState({ page: "details" });
    } else if (this.state.page === "details") {
      this.setState({ page: "list" });
    }
  };

  render() {
    return (
      <div>
        <div className="altitude-container">
          <nav className="altitude-navbar">
            <div className="container-fluid">
              <div className="navbar-header">
                <p>
                  <span>Fun facts about</span>
                  <span className="header-color"> coding</span>
                </p>
              </div>
            </div>
          </nav>
        </div>
        {this.getPage()}
      </div>
    );
  }
}

export default App;
