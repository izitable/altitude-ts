import * as React from "react";
import * as Redux from "redux";
import { connect } from "react-redux";
import Posts from "./../../components/Posts/index";
import Modal from "./../../components/Modal/index";
import { actionCreators } from "./../../action/postActions";
import * as Types from "./../../types/types";

class PostList extends React.Component<
  Types.PostListMapProps & Types.PostListMapState & Types.PostPageProps,
  Types.PostListState
> {
  constructor(
    props: Types.PostListMapProps & Types.PostListMapState & Types.PostPageProps
  ) {
    super(props);
    this.state = {
      showModal: false,
      newPost: "",
      newCategory: "",
      newNickname: ""
    };
  }

  showModal = () => {
    this.setState({ showModal: true });
  };

  hideModal = () => {
    this.setState({ showModal: false });
  };

  submit = () => {
    if (
      this.state.newPost === "" ||
      this.state.newCategory === "" ||
      this.state.newNickname === ""
    ) {
      alert("Empty fields");
      return;
    }
    this.props.submitPost({
      text: this.state.newPost,
      comments: [],
      category: this.state.newCategory,
      nickname: this.state.newNickname,
      funny: 0,
      boring: 0,
      date: new Date()
    });
    this.hideModal();
  };

  handleTextFormChange = (e: React.FormEvent<HTMLTextAreaElement>) => {
    var textArea = e.target as HTMLTextAreaElement;
    this.setState({ newPost: textArea.value });
  };

  handleNicknameFormChange = (e: React.FormEvent<HTMLInputElement>) => {
    var input = e.target as HTMLInputElement;
    this.setState({ newNickname: input.value });
  };

  handleCategoryFormChange = (e: React.FormEvent<HTMLSelectElement>) => {
    var select = e.target as HTMLSelectElement;
    this.setState({ newCategory: select.value });
  };

  updatePost = (type: string, post: Types.Post) => {
    if (type === "boring") {
      var newPost = { ...post, boring: post.boring + 1 };
      this.props.updatePost(newPost);
    } else if (type === "funny") {
      var newPost = { ...post, funny: post.funny + 1 };
      this.props.updatePost(newPost);
    }
  };

  isModalVisible = () => {
    if (this.state.showModal) {
      var { options } = this.props;
      return (
        <div className="altitude-DialogContainer">
          <Modal
            cancel={this.hideModal}
            submit={this.submit}
            options={options}
            handleTextChange={this.handleTextFormChange}
            handleNicknameChange={this.handleNicknameFormChange}
            handleCategoryChange={this.handleCategoryFormChange}
          />
        </div>
      );
    }
    return null;
  };

  goToComments = (post: Types.Post) => {
    this.props.selectPost(post);
    this.props.changePage();
  };

  render() {
    var { posts } = this.props;
    return (
      <div>
        <div className="posts-container">
          <Posts
            goToComments={this.goToComments}
            updatePost={this.updatePost}
            posts={posts}
          />
          <div className="new-post">
            <a onClick={() => this.showModal()}>
              <img alt="Add Post" src={require("./../../img/plus.svg")} />
            </a>
          </div>
        </div>
        {this.isModalVisible()}
      </div>
    );
  }
}

const mapStateToProps = (state: Types.ReduxState) => ({
  posts: state.posts.posts,
  options: state.posts.categories
});

const mapDispatchToProps = (dispatch: Redux.Dispatch<Types.ReduxState>) => ({
  submitPost: (values: Types.Post) => {
    dispatch(actionCreators.submitPost(values));
  },
  updatePost: (post: Types.Post) => {
    dispatch(actionCreators.updatePost(post));
  },
  selectPost: (post: Types.Post) => {
    dispatch(actionCreators.selectPost(post));
  }
});

export default connect<
  Types.PostListMapState,
  Types.PostListMapProps,
  Types.PostPageProps
>(mapStateToProps, mapDispatchToProps)(PostList);
