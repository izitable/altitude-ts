import * as React from "react";
import * as Redux from "redux";
import { connect } from "react-redux";
import Posts from "./../../components/Posts/index";
import Comment from "./../../components/Comment/index";
import NewComment from "./../../components/NewComment/index";
import { actionCreators } from "./../../action/postActions";
import * as Types from "./../../types/types";

class PostDetails extends React.Component<
  Types.PostDetailsMapState & Types.PostDetailsMapProps & Types.PostPageProps,
  Types.PostDetailsState
> {
  constructor(
    props: Types.PostDetailsMapState &
      Types.PostDetailsMapProps &
      Types.PostPageProps
  ) {
    super(props);

    this.state = {
      newNickname: "",
      newComment: ""
    };
  }

  handleNicknameChange = (e: React.FormEvent<HTMLInputElement>) => {
    var input = e.target as HTMLInputElement;
    this.setState({ newNickname: input.value });
  };

  handleCommentChange = (e: React.FormEvent<HTMLTextAreaElement>) => {
    var textArea = e.target as HTMLTextAreaElement;
    this.setState({ newComment: textArea.value });
  };

  updatePost = (type: string, post: Types.Post) => {
    if (type === "boring") {
      var newPost = {
        ...this.props.post,
        boring: Number(post.boring + 1)
      } as Types.Post;
      this.props.updatePost(newPost);
      this.props.selectPost(newPost);
    } else if (type === "funny") {
      var newPost = { ...this.props.post, funny: post.funny + 1 } as Types.Post;
      this.props.updatePost(newPost);
      this.props.selectPost(newPost);
    }
  };

  submitComment = () => {
    if (this.state.newComment === "" || this.state.newNickname === "") {
      alert("Empty fields");
      return;
    }
    if (this.props.post !== undefined) {
      var newPost = {
        ...this.props.post,
        comments: this.props.post.comments.concat([
          {
            text: this.state.newComment,
            author: this.state.newNickname,
            date: new Date()
          }
        ])
      };
      this.props.updatePost(newPost);
      this.props.selectPost(newPost);
    }
  };

  render() {
    var { post } = this.props;
    return (
      <div>
        <ol className="breadcrumb">
          <li>
            <a onClick={() => this.props.changePage()}>
              <img
                src={require("./../../img/back.svg")}
                height={24}
                width={24}
              />{" "}
              Back
            </a>
          </li>
        </ol>
        <div className="posts-container">
          {post !== undefined ? (
            <Posts
              goToComments={() => {}}
              updatePost={this.updatePost}
              posts={[post]}
            />
          ) : (
            undefined
          )}
        </div>
        <div className="container-comments">
          <h4>{post !== undefined ? post.comments.length : 0} Comments</h4>
          {post !== undefined
            ? post.comments.map((item: Types.Comment) => (
                <Comment comment={item} />
              ))
            : undefined}
          <div>
            <NewComment
              handleCommentChange={this.handleCommentChange}
              handleNicknameChange={this.handleNicknameChange}
              submitComment={this.submitComment}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: Types.ReduxState) => ({
  post: state.posts.selectedPost
});

const mapDispatchToProps = (dispatch: Redux.Dispatch<Types.ReduxState>) => ({
  updatePost: (post: Types.Post) => {
    dispatch(actionCreators.updatePost(post));
  },
  selectPost: (post: Types.Post) => {
    dispatch(actionCreators.selectPost(post));
  }
});

export default connect<
  Types.PostDetailsMapState,
  Types.PostDetailsMapProps,
  Types.PostPageProps
>(mapStateToProps, mapDispatchToProps)(PostDetails);
