//PROPS AND STATE FOR APP.TSX
export interface AppState {
  page: string;
}

export interface AppProps {}

//TYPES FOR ACTION AND REDUX STATE

export type Action = {
  type: string;
  payload: Post;
};

export type Post = {
  text: string;
  comments: Array<Comment>;
  category: string;
  nickname: string;
  funny: number;
  boring: number;
  date: Date;
};

export type Comment = {
  text: string;
  author: string;
  date: Date;
};

export type PostsState = {
  posts: Array<Post>;
  categories: Array<string>;
  selectedPost?: Post;
};

export type ReduxState = {
  posts: PostsState;
};

//PROPS PASSED FROM APP TO SCREENS
export interface PostPageProps {
  changePage: () => void;
}

//PROPS AND STATE FOR POSTDETAILS
export interface PostDetailsReduxProps {
  updatePost: (post: Post) => void;
  selectPost: (post: Post) => void;
  post?: Post;
}

export interface PostDetailsState {
  newNickname: string;
  newComment: string;
}

export type PostDetailsMapState = {
  post?: Post;
};

export type PostDetailsMapProps = {
  updatePost(post: Post): void;
  selectPost(post: Post): void;
};

//PROPS AND STATE FOR POSTLIST
export interface PostListState {
  showModal: boolean;
  newPost: string;
  newCategory: string;
  newNickname: string;
}

export type PostListMapState = {
  posts: Array<Post>;
  options: Array<string>;
};

export type PostListMapProps = {
  submitPost(values: Post): void;
  updatePost(post: Post): void;
  selectPost(post: Post): void;
};

export interface PostListReduxProps {
  submitPost: Function;
  updatePost: Function;
}

//PROPS FOR COMMENT COMPONENT
export interface CommentProps {
  comment: Comment;
}

//PROPS FOR MODAL COMPONENT
export type ModalProps = {
  handleTextChange: Function;
  options: Array<string>;
  submit: Function;
  cancel: Function;
  handleNicknameChange: Function;
  handleCategoryChange: Function;
};

//PROPS FOR NEW COMMENT COMPONENT
export interface NewCommentProps {
  handleNicknameChange: Function;
  handleCommentChange: Function;
  submitComment: Function;
}

//PROPS FOR POSTITEM COMPONENT
export interface PostItemProps {
  post: Post;
  updatePost?: Function;
  goToComments?: Function;
}

//PROPS FOR POSTS COMPONENT
export type PostsProps = {
  posts: Array<Post>;
  updatePost?: Function;
  goToComments?: Function;
};
