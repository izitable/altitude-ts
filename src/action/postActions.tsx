import * as Types from './../types/types';

export const SUBMIT_POST = 'SUBMIT_POST';
export const UPDATE_POST = 'UPDATE_POST';
export const SELECT_POST = 'SELECT_POST';

export type Actions = {
  SUBMIT_POST: {
    type: typeof SUBMIT_POST;
    payload: Types.Post;
  };
  UPDATE_POST: {
    type: typeof UPDATE_POST;
    payload: Types.Post;
  };
  SELECT_POST: {
    type: typeof SELECT_POST;
    payload: Types.Post;
  };
};

export const actionCreators = {
  submitPost: (values: Types.Post): Actions[typeof SUBMIT_POST] => ({
    type: SUBMIT_POST,
    payload: values
  }),
  updatePost: (post: Types.Post): Actions[typeof UPDATE_POST] => ({
    type: UPDATE_POST,
    payload: post
  }),
  selectPost: (post: Types.Post): Actions[typeof SELECT_POST] => ({
    type: SELECT_POST,
    payload: post
  })
};
